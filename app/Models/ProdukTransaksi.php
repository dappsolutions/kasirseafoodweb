<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukTransaksi extends Model
{
    protected $table = "produk_transaksi";

    public function detail(){
        return $this->hasMany('App\Models\Produk_transaksi_detail', 'produk_transaksi');
    }

    public function detail_lain(){
        return $this->hasMany('App\Models\Produk_transaksi_lain', 'produk_transaksi');
    }
    
    public function payment(){
        return $this->hasMany('App\Models\Produk_transaksi_payment', 'produk_transaksi');
    }

    public function last_status(){
        return $this->hasMany('App\Models\Produk_transaksi_status', 'produk_transaksi')->latest('id');
    }
    
    public function summary(){
        return $this->hasMany('App\Models\Produk_transaksi_summary', 'produk_transaksi');
    }
}
