<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function delete(){
        $this->deleted = 1;
        $this->save(); 
    }
}
