<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk_transaksi_detail extends Model
{
    protected $table = "produk_transaksi_detail";

    public function header(){
        return $this->belongsTo('App\Models\ProdukTransaksi', 'id');
    }
}
