<?php

namespace App\Helpers;

use App\Models\Ujian;
use Illuminate\Support\Facades\DB;

class NoGenerator
{

    private function digit_count($length, $value)
    {
        while (strlen($value) < $length)
            $value = '0' . $value;
        return $value;
    }

    public function generateKodeUjian()
    {

        $no_ujian = 'U' . date('d') . date('m') . date('Y');
        $data_ujian = Ujian::where('kode_ujian', 'like', '%' . $no_ujian . '%')
            ->orderBy('id', 'desc')->first();
        $seq = 1;

        if (!empty($data_ujian)) {
            $data = $data_ujian->toArray();
            $seq = intval(str_replace($no_ujian, '', $data['kode_ujian']));
            $seq += 1;
        }
        $seq = $this->digit_count(3, $seq);
        $no_ujian .= $seq;
        return $no_ujian;
    }
}
