<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "pembelian/simpan",
        "pembelian/selesaiProses",
        "pembelian/editJumlahOrgMakan",
        "pembelian/deleteLaporan",
        "pembelian/getListLaporanBulanan",
        "pembelian/deleteTransaksi",
        "pembelian/deleteTransaksiItem",
        "pembelian/deleteTransaksiItemLain",
        "pembelian/prosesBayar",
        "pembelian/addPesanan",
        "produk/simpan",
        "produk/deleteData",
        "produk/simpanStok",
        "produk/deleteLaporanStok",
        "menu/deleteData",
        "menu/simpan",
        "pengeluaran/simpan",
        "product_stok/simpan",
        "product_stok/simpanStok",
        "login/sign_in"
    ];
}
