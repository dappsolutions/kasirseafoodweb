<?php

namespace App\Http\Controllers\Pembelian;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Produk;
use App\Models\Produk_transaksi_detail;
use App\Models\ProdukTransaksi;
use App\Models\Transactions;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class PembelianController extends Controller
{
    public $module = "dashboard";
    public $no_trans = "";
    public $date = "";
    public $no_tambahan = "";

    public function getHeaderCss()
    {
        return array(
            'js' => asset('assets/js/url.js'),
            'js' => asset('assets/js/message.js'),
            'js' => asset('assets/js/controllers/dashboard.js'),
        );
    }

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index(Request $req)
    {
        $data['username'] = 'Dodik Rismawan';
        $data['title_top'] = 'Main';

        // $user = User::all();

        // DB::enableQueryLog();
        // $data_transaksi = ProdukTransaksi::findOrFail(38);
        // $data_transaksi_detail = $data_transaksi->detail;
        // $data_transaksi_lain = $data_transaksi->detail_lain;
        // $data_transaksi_payment = $data_transaksi->payment;
        // $data_transaksi_status = $data_transaksi->last_status()->first();
        // $data_transaksi_summary = $data_transaksi->summary;
        // $query = DB::getQueryLog();
        //         $header = Produk_transaksi_detail::findOrFail(43)->header;

        // $data_transaksi = ProdukTransaksi::with('detail', 'detail_lain')->where('produk_transaksi.id', '=', 38)->get();

        // echo '<pre>';
        // print_r($data_transaksi->toArray());die;
        // $user = User::findOrFail(3);
        // $user->delete();
        // $user->username = "tes";
        // $user->save();
        // echo '<pre>';
        // print_r($user);die;

        // $user = new User;
        // $user->username = "dodik";
        // $user->password = "dodik";
        // $user->save();
        // echo $user->id;die;

        // DB::beginTransaction();
        // try {
        //     $user = new User;
        //     $user->username = "dodik";
        //     $user->password = "dodik";
        //     $user->save();

        //     $produk = new Produk;
        //     $produk->jenis = "coba";
        //     $produk->save();

        //     DB::commit();
        // } catch (\Exception $th) {
        //     DB::rollback();
        //     echo $th->getMessage();die;
        // }

        $content['data'] = "DASHBOARD";
        $view = view("dashboard.dashboard", $content);

        $data['view_file'] = $view;
        $data['title_content'] = 'Dashboard';
        $data['module'] = $this->module;
        $data['header_data'] = $this->getHeaderCss();
        return view("template.main", $data);
    }


    public function digit_count($length, $value)
    {
        while (strlen($value) < $length)
            $value = '0' . $value;
        return $value;
    }

    public function generateNoTransaksi()
    {
        $this->no_trans = 'TRANS' . date('y') . strtoupper(date('M'));
        $data = DB::table('produk_transaksi')
            ->select(
                "*"
            )->where(function ($query) {
                $query->Where('no_transaksi', 'like', '%' . $this->no_trans . '%');
            })
            ->orderBy('id', 'desc')->get();

        $seq = 1;
        if (!empty($data->toArray())) {
            $data = $data->first();
            $seq = str_replace($this->no_trans, '', $data->no_transaksi);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(3, $seq);
        $this->no_trans .= $seq;
        echo $this->no_trans;
    }

    public function generateNoTambahan($no_transaksi = "")
    {
        $this->no_tambahan = $no_transaksi . '-';
        $data = DB::table('produk_transaksi as pd')
            ->select(
                "*"
            )->where(function ($query) {
                $query->Where('ptd.no_tambahan', 'like', '%' . $this->no_tambahan . '%');
            })
            ->join("produk_transaksi_detail as ptd", "ptd.produk_transaksi", "=", "pd.id")
            ->orderBy('ptd.id', 'desc')->get();

        $seq = 1;
        if (!empty($data->toArray())) {
            $data = $data->first();
            $seq = str_replace($this->no_tambahan, '', $data->no_tambahan);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(2, $seq);
        $this->no_tambahan .= $seq;
        echo $this->no_tambahan;
    }

    public function simpanTransksi(Request $req)
    {
        $is_valid = false;
        $message = "";

        // echo '<pre>';
        // print_r($req->all());
        // die;
        $total_detail = 0;
        try {
            $data = DB::table('produk_transaksi as pt')
                ->select(
                    "pt.*",
                    "ptd.qty_org"
                )
                ->join("produk_transaksi_detail as ptd", "ptd.produk_transaksi", "=", "pt.id")
                ->where("pt.no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            $total_org = 0;
            if (empty($data->toArray())) {
                $push = array();
                $push['no_transaksi'] = $req['no_nota'];
                $push['no_antrian'] = $req['no_antrian'];
                $push['createddate'] = date('Y-m-d H:i:s');
                $produk_transaksi = DB::table('produk_transaksi')->insertGetId($push);
            } else {
                $data = $data->first();
                $produk_transaksi = $data->id;

                $total_org = $data->qty_org;
            }


            $req['jml_org'] = $req['jml_org'] == '' ? 0 : $req['jml_org'];
            $total_org += $req['jml_org'];
            // echo $total_org;
            // die;

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['no_tambahan'] = $req['no_tambahan'];
            $push['menu'] = $req['menu'];
            $push['produk'] = $req['produk'];
            $push['qty_org'] = $total_org;
            $push['qty_produk'] = $req['jml_ikan'];
            $push['berat_produk'] = $req['berat_ikan'];
            $push['createddate'] = date('Y-m-d H:i:s');
            $push['keterangan'] = $req['keterangan'];
            if ($req['no_tambahan'] != '') {
                $push['qty_org_tambahan'] = $req['jml_org'];
            }
            DB::table('produk_transaksi_detail')->insert($push);


            $push = array();
            $push['qty_org'] = $total_org;
            DB::table('produk_transaksi_detail')->where('produk_transaksi', '=', $produk_transaksi)->update($push);


            //insert stok keluar
            $data_stok = DB::table("produk_stok as ps")
                ->select(
                    'ps.*'
                )
                ->join("produk as pd", "pd.id", "=", "ps.produk")
                ->where("pd.id", "=", $req['produk'])
                ->orderBy("pd.id", "desc")
                ->get();
            if (!empty($data_stok->toArray())) {
                $data_stok = $data_stok->first();
                $push = array();
                $push['produk_stok'] = $data_stok->id;
                $push['stok'] = $req['berat_ikan'];
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('produk_stok_histori')->insert($push);
            }


            $detail_item = DB::table('produk_transaksi_detail')
                ->select(
                    "*"
                )->where("produk_transaksi", "=", $produk_transaksi)->get();

            $total_detail = count($detail_item->toArray());
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
                'total_item' => $total_detail
            )
        );
    }


    public function selesaiProses(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['status'] = 'DAPUR';
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_transaksi_status')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteLaporan(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['status'] = 'HAPUS';
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_transaksi_status')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function editJumlahOrgMakan(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['qty_org'] = $req['qty_org'];
            DB::table('produk_transaksi_detail')->where('produk_transaksi', '=', $produk_transaksi)->update($push);
            $is_valid = true;
            $message = "Berhasil di Rubah";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteTransaksi(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['status'] = 'HAPUS TRANSAKSI';
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_transaksi_status')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteTransaksiItem(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $id = $req['id'];
        $is_valid = false;
        $message = "";
        try {
            DB::table('produk_transaksi_detail')->where('id', $id)->delete();
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteTransaksiItemLain(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $id = $req['id'];
        $is_valid = false;
        $message = "";
        try {
            DB::table('produk_transaksi_lain')->where('id', $id)->delete();
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function cekData()
    {
        $data_belum_transfer = DB::table("produk_transaksi as pt")
            ->select(
                "pt.*",
                "pts.status",
                "ptd.berat_produk",
                "ph.harga",
                "ptd.qty_org",
                "ptd.qty_org_tambahan"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_payment as ptp', 'ptp.produk_transaksi', '=', 'pt.id')
            ->join("produk_transaksi_detail as ptd", 'ptd.produk_transaksi', '=', 'pt.id')
            ->join("produk as pd", "pd.id", "=", "ptd.produk")
            ->join("menu as mn", "mn.id", "=", "ptd.menu")
            ->join("produk_harga as ph", "ph.produk", "=", "pd.id")
            ->where("pts.status", "=", "DAPUR")
            // ->where("pt.no_transaksi", "=", "TRANS20SEP028")
            ->get();
        echo '<pre>';
        print_r($data_belum_transfer);
    }

    public function prosesBayar(Request $req)
    {
        // echo '<pre>';
        // print_r($req->all());die;
        // echo $req['no_nota'];die;
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['bayar'] = $req['uang_bayar'];
            $push['kembalian'] = $req['kembalian'];
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_transaksi_payment')->insert($push);
            $is_valid = true;
            $message = "Berhasil di proses";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function addPesanan(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $data = DB::table('produk_transaksi')
                ->select(
                    "*"
                )->where("no_transaksi", "=", $req['no_nota'])->get();

            $produk_transaksi = "";
            if (!empty($data->toArray())) {
                $data = $data->first();
                $produk_transaksi = $data->id;
            }

            $push = array();
            $push['produk_transaksi'] = $produk_transaksi;
            $push['menu'] = $req['menu'];
            $push['qty'] = $req['qty'];
            $push['harga'] = $req['harga'];
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_transaksi_lain')->insert($push);

            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function getDataPembayaran($no_trans = "")
    {
        $data = DB::table("produk_transaksi_payment as ptp")
            ->select(
                'ptp.*'
            )
            ->join("produk_transaksi as pt", "pt.id", "=", "ptp.produk_transaksi")
            ->where("pt.no_transaksi", "=", $no_trans)
            ->orderBy("pt.id", "desc")
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;
        $result['bayar'] = "0";
        $result['kembalian'] = "0";
        $result['paid'] = "0";
        if (!empty($data->toArray())) {
            $data = $data->first();
            $result['bayar'] = $data->bayar;
            $result['kembalian'] = $data->kembalian;
            $result['paid'] = "1";
        }

        return $result;
    }

    public function getListPesanan($no_trans = "1")
    {
        $data = DB::table("produk_transaksi_detail as ptd")
            ->select(
                'ptd.id',
                "pd.jenis as jenis_produk",
                "mn.jenis as jenis_menu",
                "ptd.qty_org",
                "ptd.qty_produk",
                "ptd.berat_produk",
                "ph.harga",
                "ptd.keterangan",
                "pt.createddate"
            )
            ->join("produk as pd", "pd.id", "=", "ptd.produk")
            ->join("produk_transaksi as pt", "pt.id", "=", "ptd.produk_transaksi")
            ->join("menu as mn", "mn.id", "=", "ptd.menu")
            ->join("produk_harga as ph", "ph.produk", "=", "pd.id")
            ->where("pt.no_transaksi", "=", $no_trans)
            ->orderBy("pt.id", "desc")
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;

        $data_pembayaran = $this->getDataPembayaran($no_trans);

        $result = array();
        $total_porsi = 0;
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $value->total_harga = $value->harga * $value->berat_produk;
                $value->jenis_produk .=  ' ';
                // if ($key > 0) {
                //     $value->qty_org = "";
                // }
                $value->berat_produk = number_format($value->berat_produk, 2);
                $value->total_harga = number_format($value->total_harga, 0, ',', '.');
                $value->total_harga = str_replace('.', '', $value->total_harga);
                $total_porsi = $value->qty_org * 10000;
                $value->keterangan = $value->keterangan == '' ? '-' : $value->keterangan;
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
            'bayar' => $data_pembayaran['bayar'],
            'kembalian' => $data_pembayaran['kembalian'],
            'paid' => $data_pembayaran['paid'],
            'total_porsi' => $total_porsi
        ));
    }

    public function getLaporanStok()
    {
        $data = DB::table("produk_transaksi as pt")
            ->select(
                "pt.id",
                "pt.no_transaksi",
                "ptd.id as item_id",
                "pdk.jenis as menu",
                "ptd.qty_produk as qty_produk",
                "ptd.berat_produk as berat_produk",
                "ph.harga as harga_per_kg",
                "pts.status"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_payment as ptp', 'ptp.produk_transaksi', '=', 'pt.id')
            ->join('produk_transaksi_detail as ptd', 'ptd.produk_transaksi', '=', 'pt.id')
            ->join('produk as pdk', 'pdk.id', '=', 'ptd.produk')
            ->join("produk_harga as ph", "ph.produk", "=", "pdk.id")
            ->where("pts.status", "=", "SELESAI")
            ->get();

        $total_all = 0;
        $result = array();
        if (!empty($data->toArray())) {
            $temp = array();
            foreach ($data->toArray() as $key => $value) {
                $jenis_ikan = $value->menu;
                if (!in_array($jenis_ikan, $temp)) {
                    $total_berat_produk = 0;
                    $total_harga = 0;
                    $total_qty_produk = 0;
                    foreach ($data->toArray() as $v_item) {
                        $jenis_ikan_detail = $v_item->menu;
                        if ($jenis_ikan == $jenis_ikan_detail) {
                            $total_berat_produk += $v_item->berat_produk;
                            $total_harga += ($v_item->harga_per_kg * $v_item->berat_produk);
                            $total_qty_produk += $v_item->qty_produk;
                        }
                    }

                    $value->total_berat_produk = number_format($total_berat_produk, 2);
                    $value->total_harga = number_format($total_harga);
                    $value->total_qty_produk = $total_qty_produk;
                    $value->harga_per_kg = number_format($value->harga_per_kg);
                    $total_all += $total_harga;
                    array_push($result, $value);
                    $temp[] = $jenis_ikan;
                }
            }
        }

        echo json_encode(array(
            'data' => $result,
            'total_all' => number_format($total_all)
        ));
    }



    public function getListPesananByNoTambahan($no_trans = "1", $no_tambahan = "1")
    {
        $data = DB::table("produk_transaksi_detail as ptd")
            ->select(
                'ptd.id',
                "pd.jenis as jenis_produk",
                "mn.jenis as jenis_menu",
                "ptd.qty_org",
                "ptd.qty_org_tambahan",
                "ptd.qty_produk",
                "ptd.berat_produk",
                "ph.harga",
                "ptd.keterangan"
            )
            ->join("produk as pd", "pd.id", "=", "ptd.produk")
            ->join("produk_transaksi as pt", "pt.id", "=", "ptd.produk_transaksi")
            ->join("menu as mn", "mn.id", "=", "ptd.menu")
            ->join("produk_harga as ph", "ph.produk", "=", "pd.id")
            ->where("pt.no_transaksi", "=", $no_trans)
            ->where("ptd.no_tambahan", "=", $no_tambahan)
            ->orderBy("pt.id", "desc")
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;

        $data_pembayaran = $this->getDataPembayaran($no_trans);

        $result = array();
        $total_porsi = 0;
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $value->total_harga = $value->harga * $value->berat_produk;
                $value->jenis_produk .=  ' ' . $value->jenis_menu;
                // if ($key > 0) {
                //     $value->qty_org = "";
                // }

                $total_porsi = $value->qty_org * 10000;
                $value->qty_org = $value->qty_org_tambahan == '0'  ? $value->qty_org : $value->qty_org_tambahan;
                if ($value->qty_org_tambahan == '0') {
                    $value->qty_org = '-';
                }

                $value->keterangan = $value->keterangan == '' ? '-' : $value->keterangan;
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
            'bayar' => $data_pembayaran['bayar'],
            'kembalian' => $data_pembayaran['kembalian'],
            'paid' => $data_pembayaran['paid'],
            'total_porsi' => $total_porsi
        ));
    }

    public function getListPesananLain($no_trans = "")
    {
        $data = DB::table("produk_transaksi_lain as ptl")
            ->select(
                'ptl.id',
                "ptl.menu",
                "ptl.harga",
                "ptl.qty"
            )
            ->join("produk_transaksi as pt", "pt.id", "=", "ptl.produk_transaksi")
            ->where("pt.no_transaksi", "=", $no_trans)
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;

        $result = array();
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $qty = $value->qty == '' ? 0 : $value->qty;
                $value->total_harga = $qty * $value->harga;
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
        ));
    }


    public function getListAntrianTagihan()
    {
        $data = DB::table("produk_transaksi as pt")
            ->select(
                "pt.*",
                "pts.status",
                "ptd.qty_org"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_detail group by produk_transaksi", "item_max", "item_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_detail as ptd', function ($join) {
                $join->on('ptd.id', '=', 'item_max.id');
            })
            ->where("pts.status", "=", "DAPUR")
            ->get();

        $result = array();
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $value->no_antrian = $value->no_antrian == '' ? '-' : $value->no_antrian;
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
        ));
    }

    public function getListLaporan()
    {
        $data = DB::table("produk_transaksi as pt")
            ->select(
                "pt.id",
                'pt.no_transaksi',
                'pt.no_antrian',
                'ptsm.createddate',
                "pts.status",
                "ptsm.total"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_summary as ptsm', 'ptsm.produk_transaksi', '=', 'pt.id')
            ->where("pts.status", "=", "SELESAI")
            ->get();

        $result = array();
        $total = 0;
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $total += $value->total;
                $value->total = number_format($value->total);
                $value->no_antrian = $value->no_antrian == '' ? '-' : $value->no_antrian;
                array_push($result, $value);
            }
        }

        // echo '<pre>';
        // print_r($result);
        // die;
        echo json_encode(array(
            'data' => $result,
            'total' => number_format($total)
        ));
    }


    public function getListLaporanBulanan(Request $req)
    {
        $bulan = $req['bulan'];
        $tahun = $req['tahun'];

        switch (strtolower($bulan)) {
            case 'januari':
                $bulan = "01";
                break;
            case 'februari':
                $bulan = "02";
                break;
            case 'maret':
                $bulan = "03";
                break;
            case 'april':
                $bulan = "04";
                break;
            case 'mei':
                $bulan = "05";
                break;
            case 'juni':
                $bulan = "06";
                break;
            case 'juli':
                $bulan = "07";
                break;
            case 'agustus':
                $bulan = "08";
                break;
            case 'september':
                $bulan = "09";
                break;
            case 'oktober':
                $bulan = "10";
                break;
            case 'november':
                $bulan = "11";
                break;
            case 'desember':
                $bulan = "12";
                break;
            default:
                # code...
                break;
        }


        $this->date = $tahun . '-' . $bulan;

        // DB::enableQueryLog();
        $data = DB::table("produk_transaksi as pt")
            ->select(
                "pt.id",
                'pt.no_transaksi',
                'pt.no_antrian',
                'ptsm.createddate',
                "pts.status",
                "ptsm.total"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_summary as ptsm', 'ptsm.produk_transaksi', '=', 'pt.id')
            // ->where("pts.status", "=", "SELESAI")
            // ->orWhere('pts.status', 'HAPUS')
            ->where(function ($query) {
                $query->Where('pt.createddate', 'like', '%' . $this->date . '%');
            })
            ->orderBy('pt.createddate', 'asc')
            ->get();

        // $query = DB::getQueryLog();
        // echo '<pre>';
        // print_r($query);die;
        $result = array();
        $total = 0;
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $total += $value->total;
                $value->total = number_format($value->total);
                $value->no_antrian = $value->no_antrian == '' ? '-' : $value->no_antrian;
                array_push($result, $value);
            }
        }

        // echo '<pre>';
        // print_r($result);
        // die;
        echo json_encode(array(
            'data' => $result,
            'total' => number_format($total)
        ));
    }

    public function deleteAllLaporan()
    {
        $data = DB::table("produk_transaksi as pt")
            ->select(
                "pt.id",
                'pt.no_transaksi',
                'pt.no_antrian',
                'ptsm.createddate',
                "pts.status",
                "ptsm.total"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_summary as ptsm', 'ptsm.produk_transaksi', '=', 'pt.id')
            ->where("pts.status", "=", "SELESAI")
            ->get();


        $is_valid = true;
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $key => $value) {
                $push = array();
                $push['produk_transaksi'] = $value->id;
                $push['status'] = 'HAPUS';
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('produk_transaksi_status')->insert($push);
            }
        }

        echo json_encode(array(
            'is_valid' => $is_valid
        ));
    }

    public function generateTransaksi()
    {
        $data_belum_transfer = DB::table("produk_transaksi as pt")
            ->select(
                "pt.*",
                "pts.status",
                "ptd.berat_produk",
                "ph.harga",
                "ptd.qty_org",
                "ptd.qty_org_tambahan"
            )
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->join('produk_transaksi_payment as ptp', 'ptp.produk_transaksi', '=', 'pt.id')
            ->join("produk_transaksi_detail as ptd", 'ptd.produk_transaksi', '=', 'pt.id')
            ->join("produk as pd", "pd.id", "=", "ptd.produk")
            ->join("menu as mn", "mn.id", "=", "ptd.menu")
            ->join("produk_harga as ph", "ph.produk", "=", "pd.id")
            ->where("pts.status", "=", "DAPUR")
            // ->where("pt.no_transaksi", "=", "TRANS20SEP028")
            ->get();
        $data_menu_lain = DB::table("produk_transaksi_lain as ptl")
            ->select(
                'ptl.id',
                "ptl.menu",
                "ptl.harga",
                "pt.no_transaksi",
                "ptl.qty"
            )
            ->join("produk_transaksi as pt", "pt.id", "=", "ptl.produk_transaksi")
            ->joinSub("select max(id) as id, produk_transaksi from produk_transaksi_status group by produk_transaksi", "status_max", "status_max.produk_transaksi", "=", 'pt.id')
            ->join('produk_transaksi_status as pts', function ($join) {
                $join->on('pts.id', '=', 'status_max.id');
            })
            ->where("pts.status", "=", "DAPUR")
            ->get();


        // echo '<pre>';
        // print_r($data_belum_transfer->toArray());
        // die;
        $result = array();
        if (!empty($data_belum_transfer->toArray())) {
            $temp = array();
            foreach ($data_belum_transfer->toArray() as $key => $value) {
                $total = 0;
                $total_org = 0;
                $no_trans = $value->no_transaksi;
                if (!in_array($no_trans, $temp)) {
                    foreach ($data_belum_transfer->toArray() as $key => $v_detail) {
                        $no_trans_detail = $v_detail->no_transaksi;
                        if ($no_trans == $no_trans_detail) {
                            $v_detail->total_harga = $v_detail->harga * $v_detail->berat_produk;
                            $total += $v_detail->total_harga;
                        }
                    }

                    if (!empty($data_menu_lain->toArray())) {
                        foreach ($data_menu_lain->toArray() as $key => $v_detail) {
                            $no_trans_detail = $v_detail->no_transaksi;
                            if ($no_trans == $no_trans_detail) {
                                $qty = $v_detail->qty == '' ? 1 : $v_detail->qty;
                                $v_detail->total_harga = $v_detail->harga * $qty;
                                $total += $v_detail->total_harga;
                            }
                        }
                    }

                    //menu porsi
                    $total_org = $value->qty_org * 10000;
                    $total += $total_org;
                    $value->total_struk = $total;
                    array_push($result, $value);
                    $temp[] = $no_trans;
                }
            }
        }

        // echo '<pre>';
        // print_r($result);
        // die;
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $push = array();
                $push['produk_transaksi'] = $value->id;
                $push['total'] = $value->total_struk;
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('produk_transaksi_summary')->insert($push);

                $push = array();
                $push['produk_transaksi'] = $value->id;
                $push['status'] = 'SELESAI';
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('produk_transaksi_status')->insert($push);
            }
        }

        echo json_encode(array(
            'is_valid' => true,
            'message' => "Transfer Nota Berhasil"
        ));
    }

    public function getListPorsiOrg($no_trans = "")
    {
        $data = DB::table("produk_transaksi_detail as ptd")
            ->select(
                'ptd.id',
                "pd.jenis as jenis_produk",
                "mn.jenis as jenis_menu",
                "ptd.qty_org",
                "ptd.qty_org_tambahan",
                "ptd.qty_produk",
                "ptd.berat_produk",
                "ph.harga",
                "ptd.keterangan",
                "pt.createddate"
            )
            ->join("produk as pd", "pd.id", "=", "ptd.produk")
            ->join("produk_transaksi as pt", "pt.id", "=", "ptd.produk_transaksi")
            ->join("menu as mn", "mn.id", "=", "ptd.menu")
            ->join("produk_harga as ph", "ph.produk", "=", "pd.id")
            ->where("pt.no_transaksi", "=", $no_trans)
            ->orderBy("pt.id", "asc")
            ->get();

        $result = array();
        if (!empty($data->toArray())) {
            // $data_next_value = array();
            // for ($i = 0; $i < count($data->toArray()); $i++) {
            //     $value = $data->toArray()[$i]->qty_org;
            //     $next_value = $i == (count($data->toArray()) - 1) ? '0' : $data->toArray()[$i + 1]->qty_org_tambahan;
            //     echo $value . ' dan ' . $next_value . '<br/>';
            // }

            // echo '<pre>';
            // print_r($data_next_value);
            // die;
            foreach ($data->toArray() as $key => $value) {
                $value->qty_org = $value->qty_org_tambahan == '0' ? $value->qty_org : $value->qty_org_tambahan;
                array_push($result, $value->qty_org);
            }

            $data_porsi = array_unique($result);
            $result = array();
            if (!empty($data_porsi)) {
                foreach ($data_porsi as $value) {
                    $push = array();
                    $push['jml_org'] = $value;
                    array_push($result, $push);
                }

                //set qty_org pemesanan pertama
                $qty_org_first = $data_porsi[0];
                // if (count($data_porsi) > 1) {
                //     for ($i = 1; $i < count($data_porsi); $i++) {
                //         $qty_org_first -= $data_porsi[$i];
                //     }
                // }
                if (count($data_porsi) > 1) {
                    foreach ($data_porsi as $key => $v_data) {
                        if ($key != 0) {
                            $qty_org_first -= $data_porsi[$key];
                        }
                    }
                }

                $result[0]['jml_org'] = $qty_org_first;
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }
}
