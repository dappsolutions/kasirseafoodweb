<?php

namespace App\Http\Controllers\Data;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class ProdukController extends Controller
{
    public $module = "dashboard";
    public $no_trans = "";

    public function getHeaderCss()
    {
        return array(
            'js' => asset('assets/js/url.js'),
            'js' => asset('assets/js/message.js'),
            'js' => asset('assets/js/controllers/dashboard.js'),
        );
    }

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getListProduk()
    {
        $data = DB::table("produk")
            ->select('*')
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;

        $result = array();
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $value) {
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
        ));
    }

    public function getListStok()
    {
        $sql = "SELECT p.id, p.jenis, ps.stok_awal, sum_stok.stok_keluar, (ps.stok_awal - sum_stok.stok_keluar) stok_outstanding
        , ph.harga
        FROM produk p
        left join produk_stok ps
            on ps.produk = p.id
        left join
        (select sum(stok) stok_keluar, produk_stok
        from produk_stok_histori
        GROUP by produk_stok)
        sum_stok
            on sum_stok.produk_stok = ps.id
        left join (select max(id) id, produk from produk_harga group by produk) ph_max
            on ph_max.produk = p.id
        left join produk_harga ph
            on ph.id = ph_max.id
        where p.deleted = 0";
        $data = DB::select(DB::raw($sql));

        $result = array();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $value->stok_outstanding = $value->stok_outstanding == '' ? 0 : $value->stok_outstanding;
                $value->stok_awal = $value->stok_awal == '' ? 0 : $value->stok_awal;
                $value->stok_keluar = $value->stok_keluar == '' ? 0 : $value->stok_keluar;

                $value->stok_outstanding = $value->stok_awal - $value->stok_keluar;
                $value->stok_outstanding = number_format($value->stok_outstanding, 2);
                array_push($result, $value);
            }
        }


        // echo '<pre>';
        // print_r($result);
        // die;

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getLaporanPenambahanStok()
    {
        $sql = "SELECT pss.*, p.jenis as nama_produk FROM `produk_stok_supplier` pss
        join produk p
            on p.id = pss.produk
        where pss.deleted = 0
        order by pss.id desc";
        $data = DB::select(DB::raw($sql));

        $result = array();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $value->stok = number_format($value->stok);
                array_push($result, $value);
            }
        }


        // echo '<pre>';
        // print_r($result);
        // die;

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function simpan(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {

            $push = array();
            $push['jenis'] = $req['jenis'];
            $produk = DB::table('produk')->insertGetId($push);

            //harga
            $push = array();
            $push['produk'] = $produk;
            $push['produk_satuan'] = 1;
            $push['harga'] = $req['harga'];
            $produk = DB::table('produk_harga')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteData(Request $req)
    {
        $is_valid = false;
        // echo '<pre>';
        // print_r($req->all());
        // die;
        $message = "";
        try {

            $push = array();
            $push['deleted'] = 1;
            DB::table('produk')->where('id', '=', $req['id'])->update($push);
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteLaporanStok(Request $req)
    {
        $is_valid = false;
        // echo '<pre>';
        // print_r($req->all());
        // die;
        $message = "";
        try {

            $push = array();
            $push['deleted'] = 1;
            DB::table('produk_stok_supplier')->where('id', '=', $req['id'])->update($push);
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function simpanStok(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {

            $data_stok = DB::table("produk_stok as ps")
                ->select(
                    'ps.*'
                )
                ->join("produk as pd", "pd.id", "=", "ps.produk")
                ->where("pd.id", "=", $req['id'])
                ->orderBy("pd.id", "desc")
                ->get();

            $push = array();
            if (!empty($data_stok->toArray())) {
                $data_stok = $data_stok->first();
                $push['stok_awal'] = ($data_stok->stok_awal + $req['stok']);
                DB::table('produk_stok')->where('produk', "=", $req['id'])->update($push);
            } else {
                $push['produk'] = $req['id'];
                $push['stok_awal'] = $req['stok'];
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('produk_stok')->insert($push);
            }

            //insert supplier produk stok
            $push = array();
            $push['produk'] = $req['id'];
            $push['stok'] = $req['stok'];
            $push['supplier'] = $req['supplier'];
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('produk_stok_supplier')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }
}
