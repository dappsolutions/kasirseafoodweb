<?php

namespace App\Http\Controllers\Data;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class MenuController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getListMenu()
    {
        $data = DB::table("menu")
            ->select('*')
            ->where("deleted", "=", "0")
            ->get();

        // echo '<pre>';
        // print_r($data->toArray());die;

        $result = array();
        if (!empty($data->toArray())) {
            foreach ($data->toArray() as $value) {
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result,
        ));
    }

    public function simpan(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {

            $push = array();
            $push['jenis'] = $req['jenis'];
            DB::table('menu')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }

    public function deleteData(Request $req)
    {
        $is_valid = false;
        // echo '<pre>';
        // print_r($req->all());
        // die;
        $message = "";
        try {

            $push = array();
            $push['deleted'] = 1;
            DB::table('menu')->where('id', '=', $req['id'])->update($push);
            $is_valid = true;
            $message = "Berhasil di Hapus";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message,
            )
        );
    }
}
