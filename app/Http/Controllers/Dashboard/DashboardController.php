<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class DashboardController extends Controller
{
    public $module = "dashboard";

    public function getHeaderCss()
    {
        return array(
            'js' => asset('assets/js/url.js'),
            'js' => asset('assets/js/message.js'),
        );
    }

    public function index(Request $req)
    {
        // echo '<pre>';
        // print_r(session()->all());
        // die;
        $data['username'] = 'Dodik Rismawan';
        $data['title_top'] = 'Main';

        $content['data'] = "DASHBOARD";
        $view = view("dashboard.dashboard", $content);

        $data['view_file'] = $view;
        $data['title_content'] = 'Dashboard';
        $data['module'] = $this->module;
        $data['header_data'] = $this->getHeaderCss();
        return view("template.main", $data);
    }

    // public function redirectLogin()
    // {
    //     return redirect("login/sign_out");
    // }

    // public function jsonReturn()
    // {
    //     $data[0] = array('nama' => 'Dodik', 'Nim' => '1318033');
    //     $data[1] = array('nama' => 'Tejo', 'Nim' => '1318032');
    //     $data[2] = array('nama' => 'Abel', 'Nim' => '1318031');
    //     return response()->json(array(
    //         'data' => $data
    //     ));
    // }

    // public function tesCoba()
    // {
    //     return url()->current() . ' dan ' . url()->full() . ' dan ' . url()->previous();
    // }

    // public function tesSession()
    // {


    //     session(array(
    //         'username' => 'Dodik Rismawan'
    //     ));
    //     // echo '<pre>';
    //     // echo session("username");
    //     // print_r(session()->all());
    //     //deleting session
    //     // session()->flush();
    //     return session("username");
    // }

    // public function tesDb()
    // {
    //     $user = DB::select('select * from user where deleted = ?', [0]);
    //     echo '<pre>';
    //     print_r($user);
    //     die;
    // }
}
