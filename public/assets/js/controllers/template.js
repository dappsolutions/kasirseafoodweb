var Template = {
    generateSidebar: function(){
        var url_link = window.location.href;
        var data_link = url_link.split('/');
        var data_module = data_link[data_link.length-1];
        var data_menu = data_module.split('&');

        var menu_active = '';
        var child_menu = '';
        $.map(data_menu, function(elm){
            var href_menu = elm;
            var data_href_menu = href_menu.split('?');
            var menu_data = data_href_menu[data_href_menu.length -1].split('=');

            switch ($.trim(menu_data[0])) {
                case 'menu':
                    menu_active = $.trim(menu_data[1]);
                    break;
                case 'child':
                    child_menu = $.trim(menu_data[1]);
                    break;
                default:
                    break;
            }
        });

        switch (child_menu) {
            case "no":
                var ul_nav = $('ul.nav');
                var link_no_child = ul_nav.find('.no-child');
                $.map(link_no_child, function(elm){
                    var nav_link = $(elm).find('a');
                    if(menu_active == nav_link.attr('module')){
                        nav_link.addClass('active');
                    }
                });
                break;
            default:
                var ul_nav = $('ul.nav');
                var link_has_child = ul_nav.find('.has-child-'+child_menu);

                link_has_child.find('a.with-sub').addClass('active');

                $.map(link_has_child.find('ul.nav-sub').find('li'), function(elm){
                    var nav_link = $(elm).find('a');
                    if(menu_active == nav_link.attr('module')){
                        nav_link.addClass('active');
                    }
                });
                break;
        }
    }
};
