<div class="sh-sideleft-menu">
    <label class="sh-sidebar-label">Navigation</label>
    <ul class="nav">
        <li class="nav-item no-child">
            <a module='dashboard' href="{{ url('dashboard?menu=dashboard&child=no') }}" class="nav-link">
                <i class="icon ion-ios-home-outline"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- nav-item -->

        <!--Lesson-->
        <li class="nav-item has-child-lesson">
            <a href="" class="nav-link with-sub">
                <i class="icon ion-clipboard"></i>
                <span>Lesson</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a module='jam_pelajaran' href="{{ url('lesson/jam_pelajaran?menu=jam_pelajaran&child=lesson') }}" class="nav-link">Jam Pelajaran</a></li>
            </ul>
        </li>

        <!--Quiz-->
        <li class="nav-item has-child-quiz">
            <a href="" class="nav-link with-sub">
                <i class="icon ion-clipboard"></i>
                <span>Quiz</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a module='quiz_siswa' href="{{ url('quiz/quiz_siswa?menu=quiz_siswa&child=quiz') }}" class="nav-link">Quiz Siswa</a></li>
            </ul>
        </li>
    </ul>
</div><!-- sh-sideleft-menu -->


<script type="text/javascript">
    Template.generateSidebar();
</script>
