<div class="sh-breadcrumb">
    <nav class="breadcrumb">
     <a class="breadcrumb-item" href="index.html">Shamcey</a>
     <span class="breadcrumb-item active">Dashboard</span>
    </nav>
   </div><!-- sh-breadcrumb -->
   <div class="sh-pagetitle">
    <div class="input-group">
     <input type="search" class="form-control" placeholder="Search">
     <span class="input-group-btn">
      <button class="btn"><i class="fa fa-search"></i></button>
     </span><!-- input-group-btn -->
    </div><!-- input-group -->
    <div class="sh-pagetitle-left">
     <div class="sh-pagetitle-icon"><i class="icon ion-ios-home"></i></div>
     <div class="sh-pagetitle-title">
      <span>All Features Summary</span>
      <h2>Dashboard</h2>
     </div><!-- sh-pagetitle-left-title -->
    </div><!-- sh-pagetitle-left -->
   </div>
