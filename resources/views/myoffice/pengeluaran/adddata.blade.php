<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="form-layout">
                    <div class="row mg-b-25">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Keterangan : <span class="tx-danger">*</span></label>
                                <textarea name="" id="keterangan" class="form-control required" error="Keterangan">{{ isset($keterangan) ? $keterangan : '' }}</textarea>
                            </div>
                        </div><!-- col-4 -->

                    </div><!-- row -->

                    <div class="row mg-b-25">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Jumlah : <span class="tx-danger">*</span></label>
                                <input type="number" class="form-control required" error="Jumlah" id="total">
                            </div>
                        </div><!-- col-4 -->

                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <div class="text-right">
                            <button class="btn btn-success mg-r-5" onclick="Pengeluaran.submit()">Submit Form</button>
                            <button class="btn btn-secondary" onclick="Pengeluaran.cancel()">Batal</button>
                        </div>
                    </div><!-- form-layout-footer -->
                </div>
            </div>

        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-12">

    </div>
</div>
